Assignment 7
------------

- Main.java (example HTTP server, check comments for details)

Test examples
------------

- wget localhost:5050/index.html (get index page using command line)
- http://localhost:5050/index.html (get index page in browser)

Test environment
----------------

- os lubuntu 16.04 kernel version 4.13.0
- javac version 11.0.5
