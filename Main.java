import java.io.*;
import java.net.*;
import java.util.Scanner;

/**
*
* Main class
*
* example HTTP server
*
* ----- compile -----
*
* java Main.java
*
* ----- run example -----
*
* javac Main 5050
*
*/
public class Main {
    public static void main(String[] args) {
        try {
            int port = Integer.parseInt(args[0]);
            // The port to listen on
            ServerSocket ss = new ServerSocket(port);
            // Create a socket to listen
            for(;;) {
                // Loop forever
                Socket client = ss.accept();
                // Wait for a connection
                ClientThread t = new ClientThread(client); // A thread to handle it
                t.start();
                // Start the thread running
            } // Loop again
        }
        catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println("Usage: java HttpMirror <port>;");
        }
    }
    static class ClientThread extends Thread {
        Socket client;
        ClientThread(Socket client) { this.client = client; }
        public void run() {
            try {
                // Get streams to talk to the client
                BufferedReader in =
                new BufferedReader(new InputStreamReader(client.getInputStream()));
                PrintWriter out =
                new PrintWriter(new OutputStreamWriter(client.getOutputStream()));
                String header="HTTP/1.0 %d\r\nContent-Type: %s\r\n\r\n";
                int maxLines=49;
                int count=0;
                int httpSuccess=200;
                int httpNotFound=404;
                String fileName;
                String fileExtension;
                String contentType;
                String fileNameNotFound="notfound.html";
                String line[]=new String[maxLines]; // put request lines into array
                boolean fileNameExist=false;
                while((line[count++] = in.readLine()) != null) { // read data from client
                    if (line[count-1].length() == 0) break;
                    // out.println(line[count-1]);
                }
                fileName=this.getFileName(line);
                fileExtension=this.getFileExtension(fileName);
                contentType=this.getContentType(fileExtension);
                fileNameExist=this.checkFileNameExist(fileName); // check if file exists
                if(fileNameExist){
                    // Send an HTTP response header to the client
                    out.print(String.format(header,httpSuccess,contentType));
                    // Stop when we read the blank line from the client that marks
                    // the end of the request and its headers.
                    this.printFile(out,fileName);
                }else{
                    fileExtension=this.getFileExtension(fileNameNotFound);
                    contentType=this.getContentType(fileExtension);
                    out.print(String.format(header,httpNotFound,contentType));
                    this.printFile(out,fileNameNotFound);
                }
                out.close();
                in.close();
                client.close();
            }
            catch (IOException e) { /* Ignore exceptions */ }
        }
        public String getFileName(String line[]){ // get requested file name
            String arr[]=line[0].split(" ");
            return arr[1].substring(arr[1].indexOf("/")+1);
        }
        public String getFileExtension(String fileName){ // get file extension e.g. .txt
            return fileName.substring(fileName.indexOf(".")+1);
        }
        public String getContentType(String fileExtension){ // get content type for client
            String line="";
            if(fileExtension.equals("html")){
                line="text/html";
            } else if(fileExtension.equals("txt")){
                line="text/plain";
            }
            return line;
        }
        public boolean checkFileNameExist(String fileName){ // check if file name exist local directory
            return new File(fileName).exists();
        }
        public void printFile(PrintWriter out,String fileName){ // print file content
            String line;
            Scanner scanner;
            try{
                scanner=new Scanner(new File(fileName));
                for(;scanner.hasNext();){ // keep reading lines
                    line=scanner.next();
                    out.println(line);
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }
}
